var InstallPackage = require('alpi_basis').System.InstallPackage;
var ShellCommand = require('alpi_basis').System.ShellCommand;

module.exports = {
	/**
     * Percentage of cpu load.
     * 
     * @param {Function} callback
     */
	Load: function (callback) {
		InstallPackage('sysstat', (installed) => {
			if (installed === true) {
				ShellCommand('mpstat -P all 1 1 -o JSON', (cpuLoad)=>{
					callback(cpuLoad.error === null ? JSON.parse(cpuLoad.stdout.trim()).sysstat.hosts[0].statistics[0]['cpu-load'][0].usr : false);
				});
			}else{
				callback(false);
			}
		});
	},
	/**
     * Cpu Informations.
     * 
     * @param {Function} callback
     */
	Informations: function (callback) {
		InstallPackage('dmidecode', (installed)=>{
			if(installed === true){
				ShellCommand('sudo dmidecode -t 4 | awk \'NR > 6 { print }\' | tr -d \'\\t\'', (cpuInfos) => {
					if (cpuInfos.error === null) {
						var result = {};
						cpuInfos = cpuInfos.stdout.trim().split('\n');
						var prevInfo = '';
						cpuInfos.forEach(info => {
							if (info.includes(': ')) {
								prevInfo = '';
								info = info.split(': ');
								result[info[0]] = info[1];
							} else {
								if (prevInfo === '') {
									prevInfo = info;
									result[prevInfo] = [];
								} else {
									result[prevInfo].push(info);
								}
							}
						});
						callback(result);
					} else {
						callback(false);
					}
				});
			}else{
				callback(false);
			}
		});
	},
	/**
     * Get the number of cpu cores.
     * 
     * @param {Function} callback
     */
	NumberOfCore: function (callback) {
		ShellCommand('nproc', (coreNumber) => {
			callback(coreNumber.error === null ? coreNumber.stdout.trim() : false);
		});
	},
	/**
     * Check if the specified core number exists.
     * 
     * @param {String|Number} coreNumber
     * @param {Function} callback
     */
	CoreExists: function (coreNumber, callback) {
		coreNumber = Number.parseInt(coreNumber, 10);
		if(!isNaN(coreNumber) && coreNumber >= 0){
			module.exports.NumberOfCore((numberOfCore) => {
				if (numberOfCore !== false) {
					callback(coreNumber < numberOfCore);
				} else {
					callback(false);
				}
			});
		}else{
			callback(false);
		}
	},
	/**
     * Get the cpu cores load.
     * 
     * @param {Function} callback
     */
	CoresLoad: function (callback) {
		InstallPackage('sysstat', (installed)=>{
			if(installed === true){
				ShellCommand('mpstat -P ALL 1 1 | awk \'/Average:/ && $2 ~ /[0-9]/ {print $3}\'', (coresLoad) => {
					if (coresLoad.error === null) {
						var cpu = {};
						coresLoad = coresLoad.stdout.trim().split('\n');
						for (let i = 0; i < coresLoad.length; i++) {
							cpu[i] = coresLoad[i];
						}
						callback(cpu);
					} else {
						callback(false);
					}
				});
			}else{
				callback(false);
			}
		});
	},
	/**
     * Get the specified cpu core load.
     * 
     * @param {String} coreNumber
     * @param {Function} callback
     */
	CoreLoad: function (coreNumber, callback) {
		module.exports.CoreExists(coreNumber, (exists) => {
			if (exists === true) {
				module.exports.CoresLoad((load) => {
					callback(load !== false ? load[coreNumber] : false);
				});
			} else {
				callback(false);
			}
		});
	},
	/**
     * Get the cpu cores frequences.
     * 
     * @param {Function} callback
     */
	CoresFrequence: function (callback) {
		var cpu = {};
		ShellCommand('grep \'cpu MHz\' /proc/cpuinfo | awk -F \':\' \'{print substr($2, 2)}\'', (frequences) => {
			if (frequences.error === null) {
				frequences = frequences.stdout.trim().split('\n');
				for (let i = 0; i < frequences.length; i++) {
					cpu[i] = frequences[i];
				}
				callback(cpu);
			} else {
				callback(false);
			}
		});
	},
	/**
     * Get the specified cpu core frequence.
     * 
     * @param {String} coreNumber
     * @param {Function} callback
     */
	CoreFrequence: function (coreNumber, callback) {
		module.exports.CoreExists(coreNumber, (exists) => {
			if (exists === true) {
				module.exports.CoresFrequence((frequences) => {
					callback(frequences !== false ? frequences[coreNumber] : false);
				});
			} else {
				callback(false);
			}
		});
	},
	/**
     * Get the cpu cores informations.
     * 
     * @param {Function} callback
     */
	CoresInformations: function (callback) {
		ShellCommand('cat /proc/cpuinfo | tr -d \'\\t\'', (coresInformations) => {
			if (coresInformations.error === null) {
				var result = {};
				var coreNumber = 1;
				coresInformations = coresInformations.stdout.trim().split('\n\n');
				coresInformations.forEach(ram => {
					ram = ram.split('\n');
					ram.shift();
					var infos = {};
					ram.forEach(info => {
						info = info.split(': ');
						if (info[0] === 'flags' || info[0] === 'bugs') {
							info[1] = info[1].split(' ');
							infos[info[0]] = info[1];
						} else {
							infos[info[0]] = String(info[1]).trim();
						}
					});
					result[coreNumber++] = infos;
				});
				callback(result);
			} else {
				callback(false);
			}
		});
	},
	/**
     * Get the specified cpu core informations.
     * 
     * @param {String} coreNumber
     * @param {Function} callback
     */
	CoreInformations: function (coreNumber, callback) {
		module.exports.CoreExists(coreNumber, (exists) => {
			if (exists === true) {
				module.exports.CoresInformations(informations => {
					if (informations !== false) {
						callback(informations[coreNumber]);
					} else {
						callback(false);
					}
				});
			} else {
				callback(false);
			}
		});
	}
};
