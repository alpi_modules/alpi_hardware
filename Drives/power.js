var ShellCommand = require('alpi_basis').System.ShellCommand;
var InstallPackage = require('alpi_basis').System.InstallPackage;
var Exists = require('./informations').DiskExists;

module.exports = {
	/**
     * Get power state of a drive.
     * 
     * @param {String} drive
     * @param {Function} callback
     */
	State: function (drive, callback) {
		Exists(drive, (exists) => {
			if (exists === true) {
				InstallPackage('hdparm', (installed)=>{
					if(installed === true){
						ShellCommand(`sudo hdparm -C ${drive} | tr -s ' ' | grep 'is:' | cut -d':' -f2`, (state) => {
							callback(state.stderr === '' ? state.stdout.trim() : false);
						});
					}else{
						callback(false);
					}
				});
			} else {
				callback(false);
			}
		});
	},
	/**
     * Put a drive in standby.
     * 
     * @param {String} drive
     * @param {Function} callback
     */
	Standby: function (drive, callback) {
		Exists(drive, (exists) => {
			if (exists === true) {
				InstallPackage('hdparm', (installed)=>{
					if(installed === true){
						ShellCommand(`sudo hdparm -y ${drive}`, (e) => {
							callback(e.stderr === '');
						});
					}else{
						callback(false);
					}
				});
			} else {
				callback(false);
			}
		});
	},
	/**
     * Put a drive in sleep mode.
     * 
     * @param {String} drive
     * @param {Function} callback
     */
	Sleep: function (drive, callback) {
		Exists(drive, (exists) => {
			if (exists === true) {
				InstallPackage('hdparm', (installed)=>{
					if(installed === true){
						ShellCommand(`sudo hdparm -Y ${drive}`, (e) => {
							callback(e.stderr === '');
						});
					}else{
						callback(false);
					}
				});
			} else {
				callback(false);
			}
		});
	}
};
