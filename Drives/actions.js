var ShellCommand = require('alpi_basis').System.ShellCommand;
var PartitionExists = require('./informations').PartitionExists;

module.exports = {
	/**
     * Mount a partition.
     * 
     * @param {String} partition
     * @param {String} mountpoint
     * @param {Function} callback
     */
	Mount: function (partition, mountpoint, callback) {
		PartitionExists(partition, (exists) => {
			if (exists === true) {
				ShellCommand(`sudo mount ${partition} ${mountpoint}`, (e) => {
					callback(e.stderr === '');
				});
			} else {
				callback(false);
			}
		});
	},
	/**
     * Unmount a partition.
     * 
     * @param {String} partition
     * @param {Function} callback
     */
	Unmount: function (partition, callback) {
		PartitionExists(partition, (exists) => {
			if (exists === true) {
				ShellCommand(`sudo umount ${partition}`, (e) => {
					callback(e.stderr === '');
				});
			} else {
				callback(false);
			}
		});
	},
	DmCrypt: require('./dmCrypt'),
	Power: require('./power')
};
