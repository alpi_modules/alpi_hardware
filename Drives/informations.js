var ShellCommand = require('alpi_basis').System.ShellCommand;
var InstallPackage = require('alpi_basis').System.InstallPackage;

module.exports = {
	/**
	 * List all disks.
	 * 
	 * @param {Function} callback
	 */
	ListDisks: function (callback) {
		ShellCommand('lsblk -ndo "PATH"', (disks) => {
			if (disks.error === null) {
				callback(disks.stdout.trim().split('\n'));
			} else {
				callback(false);
			}
		});
	},
	/**
	 * List all disks with their partitions.
	 * 
	 * @param {Function} callback
	 */
	ListDisksWithPartitions: function (callback) {
		module.exports.ListDisks((disks) => {
			if (disks !== false) {
				Promise.all(disks.map((disk) => {
					return new Promise((resolve) => {
						ShellCommand(`lsblk -no 'PATH,TYPE' ${disk} | awk '!/disk/ {print $1}'`, (partitions) => {
							if (partitions.error === null) {
								partitions = partitions.stdout.trim().split('\n');
								resolve({
									'disk': disk,
									'partitions': partitions
								});
							} else {
								resolve(false);
							}
						});
					});
				})).then((results) => {
					if (results.every((result) => { return result !== false; })) {
						callback(results);
					} else {
						callback(false);
					}
				});
			} else {
				callback(false);
			}
		});
	},
	/**
	 * Check if a device (disk or partition) exists.
	 * 
	 * @param {String} device
	 * @param {Function} callback
	 */
	Exists: function (device, callback) {
		module.exports.ListDisksWithPartitions((devices) => {
			if (devices !== false) {
				for (let i = 0; i < devices.length; i++) {
					if (devices[i].disk === device) {
						callback(true);
						return;
					} else if (devices[i].partitions.includes(device)) {
						callback(true);
						return;
					}
				}
				callback(false);
			} else {
				callback(false);
			}
		});
	},
	/**
     * Check if a disk exists.
     * 
     * @param {String} disk,
     * @param {Function} callback
     */
	DiskExists: function (disk, callback) {
		module.exports.ListDisks((devices) => {
			if (devices !== false) {
				for (let i = 0; i < devices.length; i++) {
					if (devices[i] === disk) {
						callback(true);
						return;
					}
				}
				callback(false);
			} else {
				callback(false);
			}
		});
	},
	/**
     * Check if a partition exists.
     * 
     * @param {String} partition
     * @param {Function} callback
     */
	PartitionExists: function (partition, callback) {
		module.exports.ListDisksWithPartitions((devices) => {
			if (devices !== false) {
				for (let i = 0; i < devices.length; i++) {
					if (devices[i].partitions.includes(partition)) {
						callback(true);
						return;
					}
				}
				callback(false);
			} else {
				callback(false);
			}
		});
	},
	/**
	 * Get the type of the device (disk or partition) (disk/part/crypt/...).
	 * 
	 * @param {String} device 
	 * @param {Function} callback 
	 */
	DeviceType: function (device, callback) {
		module.exports.PartitionExists(device, (exists) => {
			if (exists) {
				ShellCommand(`lsblk -no "TYPE" ${device}`, (type) => {
					if (type.error === null) {
						type = type.stdout.trim();
						callback(type);
					} else {
						callback(false);
					}
				});
			} else {
				module.exports.DiskExists(device, (exists) => {
					if (exists) {
						callback('disk');
					} else {
						callback(false);
					}
				});
			}
		});
	},
	/**
	 * Get the transport type of the disk (sata/nvme/usb/...).
	 * 
	 * @param {disk} disk 
	 * @param {Function} callback 
	 */
	DiskTransportType: function(disk, callback){
		module.exports.DiskExists(disk, (exists)=>{
			if(exists){
				module.exports.Informations(disk, (informations)=>{
					if(informations !== false){
						if(informations.tran !== ''){
							callback(informations.tran);
						}else{
							callback(false);
						}
					}
				});
			}else{
				callback(false);
			}
		});
	},
	/**
	 * Get the subsystem of the device (scsi/mmc/nvme/... + usb)
	 * 
	 * @param {String} device
	 * @param {Function} callback
	 */
	DiskSubsystem: function(device, callback){
		module.exports.Informations(device, (informations)=>{
			if(informations !== false){
				callback(informations.subsystems);
			}else{
				callback(false);
			}
		});
	},
	/**
	 * Get informations of a device (disk or partition).
	 * 
	 * @param {String} device
	 * @param {Function} callback
	 */
	Informations: function (device, callback) {
		module.exports.DeviceType(device, (type) => {
			if (type !== false) {
				ShellCommand(`lsblk -${type === 'disk' ? 'd' : ''}PO ${device}`, (informations) => {
					if (informations.error === null) {
						informations = informations.stdout.trim().split('" ');
						var result = {};
						informations.forEach((information) => {
							information = information.split('=');
							result[information[0].toLowerCase()] = information[1].slice(1, information[1].length);
						});
						callback(result);
					} else {
						callback(false);
					}
				});
			} else {
				callback(false);
			}
		});
	},
	/**
     * Get informations of a device (disk or partition) from udev.
     * 
     * @param {String} device
     * @param {Function} callback
     */
	UdevInfos: function (device, callback) {
		module.exports.Exists(device, (exists) => {
			if (exists === true) {
				ShellCommand(`udevadm info --query=property --name=${device}`, (udevInfos) => {
					if (udevInfos.stderr === '') {
						udevInfos = udevInfos.stdout.trim().split('\n');
						var result = {};
						udevInfos.forEach(info => {
							info = info.split('=');
							result[info[0].toLowerCase()] = info[1];
						});
						callback(result);
					} else {
						callback(false);
					}
				});
			} else {
				callback(false);
			}
		});
	},
	/**
     * Get informations of a sata disk with hdparm.
     * 
     * @param {String} disk
     * @param {Function} callback
     */
	HdParmInformations: function (disk, callback) {
		module.exports.DiskTransportType(disk, (transportType)=>{
			if(transportType !== false && transportType === 'sata'){
				InstallPackage('hdparm', (installed) => {
					if (installed === true) {
						ShellCommand(`sudo hdparm -I ${disk}`, (HdParmInformations) => {
							callback(HdParmInformations.stderr === '' ? HdParmInformations.stdout.trim() : false);
						});
					} else {
						callback(false);
					}
				});
			}else{
				callback(false);
			}
		});
	},
	/**
     * Get the temperature of a disk.
     * 
     * @param {String} disk
     * @param {Function} callback
     */
	Temperature: function (disk, callback) {
		module.exports.DiskTransportType(disk, (transportType)=>{
			if(transportType !== false){
				if(transportType === 'sata'){
					InstallPackage('hdparm', (installed) => {
						if (installed === true) {
							// ShellCommand(`sudo hdparm -H ${disk} | tr -s ' ' | grep 'is:' | cut -d':' -f2`, (temperature) => {
							ShellCommand(`sudo hdparm -H ${disk} | tr -s ' ' | awk -F ':' '/is:/ {print $2}'`, (temperature) => {
								callback(temperature.stderr === '' ? temperature.stdout.trim() : false);
							});
						} else {
							callback(false);
						}
					});
				}else if(transportType === 'nvme'){
					require('./smart').NvmeSmartLog(disk, (informations)=>{
						if(informations !== false){
							callback(informations.temperature);
						}else{
							callback(false);
						}
					});
				}else{
					callback(false);
				}
			}else{
				callback(false);
			}
		});
	}
};
