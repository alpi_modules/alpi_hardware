var InstallPackage = require('alpi_basis').System.InstallPackage;
var ShellCommand = require('alpi_basis').System.ShellCommand;
var DeviceType = require('./informations').DeviceType;
var UnmountPartition = require('./actions').Unmount;

module.exports = {
	/**
	 * Check if the partition contains an encrypted container header
	 * 
	 * @param {String} partition 
	 * @param {Function} callback 
	 */
	IsLuks: function (partition, callback) {
		DeviceType(partition, (type) => {
			if (type === 'part') {
				InstallPackage('cryptsetup', (installed) => {
					if (installed) {
						ShellCommand(`sudo cryptsetup isLuks ${partition}`, (isLuks) => {
							callback(isLuks.error === null);
						});
					} else {
						callback(false);
					}
				});
			} else {
				callback(false);
			}
		});
	},
	/**
	 * Check if the container is active.
	 * 
	 * @param {String} deviceMapperName
	 * @param {Function} callback
	 */
	IsActive: function (deviceMapperName, callback) {
		DeviceType(`dev/mapper/${deviceMapperName}`, (type) => {
			if (type === 'crypt') {
				InstallPackage('cryptsetup', (installed) => {
					if (installed) {
						ShellCommand(`sudo cryptsetup status ${deviceMapperName}`, (isActive) => {
							if (isActive.error === null) {
								callback(isActive.stdout.trim().includes('is active'));
							} else {
								callback(false);
							}
						});
					} else {
						callback(false);
					}
				});
			} else {
				callback(false);
			}
		});
	},
	/**
	 * Check if the container is in use.
	 * 
	 * @param {String} deviceMapperName 
	 * @param {Function} callback 
	 */
	IsInUse: function (deviceMapperName, callback) {
		module.exports.IsActive(deviceMapperName, (isActive) => {
			if (isActive) {
				ShellCommand(`sudo cryptsetup status ${deviceMapperName}`, (isInUse) => {
					if (isInUse.error === null) {
						callback(isInUse.stdout.trim().includes('is in use'));
					} else {
						callback(false);
					}
				});
			} else {
				callback(false);
			}
		});
	},
	/**
	 * Get informations about the encrypted container
	 * 
	 * @param {String} deviceMapperName 
	 * @param {Function} callback 
	 */
	Informations: function (deviceMapperName, callback) {
		DeviceType(`/dev/mapper/${deviceMapperName}`, (type) => {
			if (type === 'crypt') {
				ShellCommand(`sudo cryptsetup status ${deviceMapperName}`, (informations) => {
					if (informations.error === null) {
						informations = informations.stdout.trim().split('\n');
						var result = { 'name': deviceMapperName };
						var state = informations.shift();
						informations.active = state.includes('is active');
						informations['in use'] = state.includes('is in use');
						informations.forEach((information) => {
							information = information.split(':');
							result[information[0].trim()] = information[1].trim();
						});
						callback(informations);
					} else {
						callback(false);
					}
				});
			} else {
				callback(false);
			}
		});
	},
	/**
	* Get the list of the availables encryption types.
	*
	* @return {Array}
	*/
	AvailableTypes: function () {
		return [null, 'luks', 'luks1', 'luks2', 'plain', 'loopaes', 'tcrypt'];
	},
	/**
	 * Get the list of the availables cipher names.
	 * 
	 * @return {Array}
	 */
	AvailableCipherNames: function () {
		return [null, 'aes', 'twofish', 'serpent', 'cast5', 'cast6'];
	},
	/**
	* Get the list of the availables cipher modes.
	*
	* @return {Array}
	*/
	AvailableCipherModes: function () {
		return [null, 'ecb', 'cbc-plain', 'xts-plain64'];
	},
	/**
	* Get the list of the availables hashes.
	*
	* @return {Array}
	*/
	AvailableHashes: function () {
		return [null, 'sha1', 'sha256', 'sha512', 'ripemd160'];
	},
	/**
	 * Create a new encrypted container with various parameters.
	 * 
	 * @param {String} partition 
	 * @param {String|null} deviceMapperName
	 * @param {String} password
	 * @param {String|null} type
	 * @param {String|null} cipherName
	 * @param {String|null} cipherMode 
	 * @param {String|null} hash 
	 * @param {Integer|null} size the key size (512 max)
	 * @param {Function} callback
	 */
	Create: function (partition, deviceMapperName, password, type, cipherName, cipherMode, hash, size, callback) {
		DeviceType(partition, (partType) => {
			if (partType === 'part') {
				InstallPackage('cryptsetup', (installed) => {
					if (installed) {
						if (deviceMapperName === null || deviceMapperName.match(/^[A-Za-z]+$/)) {
							if (module.exports.AvailableTypes.includes(type)) {
								if (module.exports.AvailableCipherNames.includes(cipherName)) {
									if (module.exports.AvailableCipherModes.includes(cipherMode)) {
										if (module.exports.AvailableHashes.includes(hash)) {
											if (size === null || (typeof size === 'number' && size <= 512)) {
												password = password.replace(/'/g, '\\\'');
												ShellCommand(`echo -n '${password}' | sudo cryptsetup luksFormat${type !== null ? ` -M ${type}` : ''}${cipherName !== null ? ` -c ${cipherName}${cipherMode !== null ? `-${cipherMode}` : ''}` : ''}${size !== null ? ` -s ${size}` : ''}${hash !== null ? ` -h ${hash}` : ''} ${partition}${deviceMapperName !== null ? ` ${deviceMapperName}` : ''} -d -`, (success) => {
													if (success.error === null) {
														module.exports.IsLuks(partition, callback);
													} else {
														callback(false);
													}
												});
											} else {
												callback(false);
											}
										} else {
											callback(false);
										}
									} else {
										callback(false);
									}
								} else {
									callback(false);
								}
							} else {
								callback(false);
							}
						} else {
							callback(false);
						}
					} else {
						callback(false);
					}
				});
			} else {
				callback(false);
			}
		});
	},
	/**
     * Open an encrypted container.
     * 
     * @param {String} partition
     * @param {String} password
     * @param {String} deviceMapperName
     * @param {Function} callback
     */
	Open: function (partition, password, deviceMapperName, callback) {
		if (deviceMapperName.match(/^[A-Za-z]+$/)) {
			module.exports.IsLuks(partition, (isLuks) => {
				if (isLuks) {
					password = password.replace(/'/g, '\\\'');
					ShellCommand(`echo -n '${password}' | sudo cryptsetup open ${partition} '${deviceMapperName}' -d -`, (e) => {
						callback(e.stderr === '');
					});
				} else {
					callback(false);
				}
			});
		} else {
			callback(false);
		}
	},
	/**
     * Close an encrypted container.
     * 
     * @param {String} deviceMapperName ()
     * @param {Function} callback
     */
	Close: function (deviceMapperName, callback) {
		module.exports.IsActive(deviceMapperName, (isActive) => {
			if (isActive) {
				new Promise((resolve) => {
					module.exports.IsInUse(deviceMapperName, (isInUse) => {
						if (isInUse) {
							UnmountPartition(`/dev/mapper/${deviceMapperName}`, (unmounted) => {
								if (unmounted) {
									resolve(true);
								} else {
									resolve(false);
								}
							});
						} else {
							resolve(true);
						}
					});
				}).then((result) => {
					if (result) {
						ShellCommand(`sudo cryptsetup close ${deviceMapperName}`, (closed) => {
							callback(closed.error === null);
						});
					} else {
						callback(false);
					}
				});
			} else {
				callback(false);
			}
		});
	}
};
