var basis = require('alpi_basis');

module.exports = {
	/**
     * Bios informations.
     * 
     * @param {Function} callback
     */
	BiosInformations: function (callback) {
		basis.System.InstallPackage('dmidecode', (installed)=>{
			if(installed === true){
				basis.System.ShellCommand('sudo dmidecode -t 0 | awk \'NR > 6 { print }\' | tr -d \'\\t\'', (biosInfos) => {
					if (biosInfos.error === null) {
						var result = {};
						biosInfos = biosInfos.stdout.trim().split('\n');
						var prevInfo = '';
						biosInfos.forEach(info => {
							if (info.includes(': ')) {
								prevInfo = '';
								info = info.split(': ');
								result[info[0]] = info[1];
							} else {
								if (prevInfo === '') {
									prevInfo = info;
									result[prevInfo] = [];
								} else {
									result[prevInfo].push(info);
								}
							}
						});
						callback(result);
					} else {
						callback(false);
					}
				});
			}else{
				callback(false);
			}
		});
	},
	/**
     * System Informations.
     * 
     * @param {Function} callback
     */
	SystemInformations: function (callback) {
		basis.System.InstallPackage('dmidecode', (installed)=>{
			if(installed === true){
				basis.System.ShellCommand('sudo dmidecode -t 1 | awk \'NR > 6 { print }\' | tr -d \'\\t\'', (systemInfos) => {
					if (systemInfos.error === null) {
						var result = {};
						systemInfos = systemInfos.stdout.trim().split('\n');
						systemInfos.forEach(info => {
							info = info.split(': ');
							result[info[0]] = info[1].trim();
						});
						callback(result);
					} else {
						callback(false);
					}
				});
			}else{
				callback(false);
			}
		});
	},
	/**
     * Base board informations.
     * 
     * @param {Function} callback
     */
	BaseBoardInformations: function (callback) {
		basis.System.InstallPackage('dmidecode', (installed)=>{
			if(installed === true){
				basis.System.ShellCommand('sudo dmidecode -t 2 | awk \'NR > 6 { print }\' | tr -d \'\\t\'', (baseBoardInfos) => {
					if (baseBoardInfos.error === null) {
						var result = {};
						baseBoardInfos = baseBoardInfos.stdout.trim().split('\n');
						var prevInfo = '';
						baseBoardInfos.forEach(info => {
							if (info.includes(': ')) {
								prevInfo = '';
								info = info.split(': ');
								result[info[0]] = info[1];
							} else {
								if (prevInfo === '') {
									prevInfo = info;
									result[prevInfo] = [];
								} else {
									result[prevInfo].push(info);
								}
							}
						});
						callback(result);
					} else {
						callback(false);
					}
				});
			}else{
				callback(false);
			}
		});
	},
	/**
     * Max ram capacity of the motherboard.
     * 
     * @param {Function} callback
     */
	MaxRamCapacity: function (callback) {
		basis.System.InstallPackage('dmidecode', (installed)=>{
			if(installed === true){
				basis.System.ShellCommand('sudo dmidecode -t 16 | awk -F \':\' \'/Maximum Capacity/ {print $2}\'', (maxRamCapacity) => {
					callback(maxRamCapacity.error === null ? maxRamCapacity.stdout.trim() : false);
				});
			}else{
				callback(false);
			}
		});
	},
	/**
     * Max ram devices of the motherboard.
     * 
     * @param {Function} callback
     */
	MaxRamDevices: function (callback) {
		basis.System.InstallPackage('dmidecode', (installed)=>{
			if(installed === true){
				basis.System.ShellCommand('sudo dmidecode -t 16 | awk -F \':\' \'/Number Of Devices/ {print $2}\'', (maxRamDevices) => {
					callback(maxRamDevices.error === null ? maxRamDevices.stdout.trim() : false);
				});
			}else{
				callback(false);
			}
		});
	},
};
