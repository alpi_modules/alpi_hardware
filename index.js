module.exports = {
	Cpu: require('./cpu.js'),
	Ram: require('./ram.js'),
	Drives: require('./drives.js'),
	Motherboard: require('./motherboard')
};
