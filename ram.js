var ShellCommand = require('alpi_basis').System.ShellCommand;

module.exports = {
	/**
	 * Ram size in kB.
	 * 
	 * @param {Function} callback
	 */
	TotalSize: function (callback) {
		ShellCommand('free  | awk \'/Mem/ {print $2}\'', (ramTotal) => {
			callback(ramTotal.error === null ? ramTotal.stdout.trim() : false);
		});
	},
	/**
     * Percentage of ram usage.
     * 
     * @param {Function} callback
     */
	Usage: function (callback) {
		ShellCommand('free | awk \'/Mem/ {print $3/$2 * 100.0}\'', (ramLoad) => {
			callback(ramLoad.error === null ? ramLoad.stdout.trim() : false);
		});
	},
	/**
	 * Ram used in kB
	 * 
	 * @param {Function} callback 
	 */
	Used: function (callback) {
		ShellCommand('free  | awk \'/Mem/ {print $3}\'', (ramUsed) => {
			callback(ramUsed.error === null ? ramUsed.stdout.trim() : false);
		});
	},
	/**
	 * Ram left in kB
	 * @param {Function} callback 
	 */
	Free: function (callback) {
		ShellCommand('free | awk \'/Mem/ {print $4}\'', (ramFree) => {
			callback(ramFree.error === null ? ramFree.stdout.trim() : false);
		});
	},
	/**
     * Ram informations.
     * 
     * @param {Function} callback
     */
	Informations: function (callback) {
		ShellCommand('sudo dmidecode -t 17 | awk \'NR > 6 { print }\' | tr -d \'\\t\'', (ramInfos) => {
			if (ramInfos.error === null) {
				var result = {};
				var slotNumber = 1;
				ramInfos = ramInfos.stdout.trim().split('\n\n');
				ramInfos.forEach(ram => {
					ram = ram.split('\n');
					ram.splice(0, 3);
					var infos = {};
					ram.forEach(info => {
						info = info.split(': ');
						infos[info[0]] = info[1].trim();
					});
					result[slotNumber++] = infos;
				});
				callback(result);
			} else {
				callback(false);
			}
		});
	}
};
